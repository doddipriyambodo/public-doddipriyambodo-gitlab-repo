
At AWS, we consider all are builders. The innovators, the collaborators, the creators. The ones who see what doesn’t exist, and then make it exist. We believe nothing should stand in the builder’s way, and dreams never have to turn off. With AWS, it’s time to build on.. Because we are aiming to build something better for the world. In this post, for you developers who would like to make your hands dirty, you can try these hands on lab which will create a sample application based on the technology that you like. Please explore and … GO BUILD!

General resources:

-   [https://infrastructure.aws/](https://infrastructure.aws/)
-   [https://thinkbig.cloud](https://thinkbig.cloud/)/ and [http://awssam.com/iday/](http://awssam.com/iday/)
-   [https://lightsailworkshop.com](https://lightsailworkshop.com/)
-   [https://ec2spotworkshops.com](https://ec2spotworkshops.com/)
-   [https://cdkworkshop.com](https://cdkworkshop.com/)
-   [https://github.com/awsdocs/elastic-beanstalk-samples](https://github.com/awsdocs/elastic-beanstalk-samples)
-   [https://github.com/doddipriyambodo/aws-workshop (fork)](https://github.com/doddipriyambodo/aws-workshop)

Serverless:

-   [https://github.com/aws-samples/aws-modern-application-workshop](https://github.com/aws-samples/aws-modern-application-workshop)
-   [https://aws.amazon.com/getting-started/projects/build-serverless-web-app-lambda-apigateway-s3-dynamodb-cognito/](https://aws.amazon.com/getting-started/projects/build-serverless-web-app-lambda-apigateway-s3-dynamodb-cognito/)
-   [https://github.com/aws-samples/aws-serverless-workshops](https://github.com/aws-samples/aws-serverless-workshops)

Containers:

-   [https://eksworkshop.com](https://eksworkshop.com/)
-   [https://containeronaws.com](https://containeronaws.com/)
-   [https://github.com/aws-asean-builders/aws-summit2019-techfest](https://github.com/aws-asean-builders/aws-summit2019-techfest)
-   [https://github.com/doddipriyambodo/aws-eks-workshop (fork)](https://github.com/doddipriyambodo/aws-eks-workshop)
-   [https://github.com/doddipriyambodo/aws-ecs-workshop](https://github.com/doddipriyambodo/aws-ecs-workshop) [(fork)](https://github.com/doddipriyambodo/aws-eks-workshop)

Amplify:

-   https://github.com/ykbryan/aws-react-native-amplify-workshop
-   https://github.com/ykbryan/aws-react-amplify-workshop
-   [https://amplify-workshop.go-aws.com](https://amplify-workshop.go-aws.com/)

Landing Zone:

-   https://github.com/tohwsw/aws-account-factory

Data Lake:

-   [http://bit.ly/sg-techfest-2019-datalake-lab](http://bit.ly/sg-techfest-2019-datalake-lab)

Machine Learning and Artificial Intelligence:

-   [https://github.com/aws-samples/amazon-personalize-samples](https://github.com/aws-samples/amazon-personalize-samples)
-   https://github.com/yudho/machine-learning-workshop
-   [https://sagemaker-workshop.com](https://sagemaker-workshop.com/)

Huge resources:

-   [https://github.com/aws-samples](https://github.com/aws-samples)
-   [https://github.com/awsdocs/](https://github.com/awsdocs/)
-   [https://github.com/aws-asean-builders](https://github.com/aws-asean-builders)
-   [https://docs.aws.amazon.com](https://docs.aws.amazon.com/)

AWS Official channel for Getting Started Guide (Step by Step), Free Trainings, and Ready to Deploy Solutions:

-   https://aws.amazon.com/getting-started/
-   https://aws.amazon.com/solutions/

_**My other Private resources:**_  _(can only be accessed if you have the credential, internal AWS account and in my personal Repository in Github, Gitlab, and CodeCommit)_

-   [http://bit.ly/2Vxf1xX](http://bit.ly/2Vxf1xX)

While talkers Talk, builders Build!

Kind Regards,  
Doddi Priyambodo
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTc2NzI5NzI2OF19
-->