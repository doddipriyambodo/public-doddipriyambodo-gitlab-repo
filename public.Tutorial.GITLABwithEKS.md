## Configuring Amazon EKS using eksctl - and configure GitLab 

### What solution that you will build today?
1. Configure Amazon EKS
2. Configure GitLab to use EKS
3. Create Helm and Tiller on EKS
4. Create Ingress that automatically deploy Load Balancer on AWS
5. Create GitLab Runner for CI/CD (if you want to use it rather than Codepipeline)
6. Create Persistent Storage for Kubernetes
7. Create Prometheus for Monitoring
8. Deploy the Kubernetes Web UI Dashboard
9. Deploy Kubernetes Metric Server using Horizontal Pod Scaler
10. Build the Guest Book application

References:
[https://docs.aws.amazon.com/eks/latest/userguide/getting-started-eksctl.html](https://docs.aws.amazon.com/eks/latest/userguide/getting-started-eksctl.html) 
[https://gitlab.com/help/user/project/clusters/eks_and_gitlab/index.md](https://gitlab.com/help/user/project/clusters/eks_and_gitlab/index.md)

Note: I am using MacOS Mojave version 10.4.6 for my laptop when I do this lab.

Step by Step:

### I. Configure Amazon EKS
1. Configure your laptop to connect to AWS environment.
	```
	sudo pip install --upgrade pip
	pip install awscli --upgrade --user
	aws configure
	```
2. Prepare the tool eksctl to configure Kubernetes on AWS platform
	```
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	brew tap weaveworks/tap
	brew install weaveworks/tap/eksctl
	brew upgrade eksctl && brew link --overwrite eksctl
	eksctl version
	```
3. Configure the Kubernetes kubectl to configure your cluster
	```
	curl -o kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.13.7/2019-06-11/bin/darwin/amd64/kubectl
	chmod +x ./kubectl
	mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$HOME/bin:$PATH
	echo 'export PATH=$HOME/bin:$PATH' >> ~/.bash_profile
	kubectl version --short --client
	```
4. Create your Kubernetes Cluster on EKS now!
	```
	eksctl version
	eksctl create cluster --help
	
	eksctl create cluster \
	--name doddi-gitlab-on-eks \
	--version 1.13 \
	--nodegroup-name standard-workers \
	--node-type t3.medium \
	--nodes 3 \
	--nodes-min 1 \
	--nodes-max 4 \
	--node-ami auto

	kubectl get svc
	```
### II. Configure GitLab to use EKS
5. Creating simple project from GitLab to Amazon EKS
	
	- On GitLab, create a new project by clicking on the `+` icon in the top navigation bar and selecting **New project**.
	- 	On the new project screen, click on the **Create from template** tab, and select "Use template" for the Ruby on Rails sample project.
	- 	Give the project a name, and then select **Create project**.
	- 	From the left side bar, hover over **Operations > Kubernetes > Add Kubernetes cluster**, then click **Add an existing Kubernetes cluster**.	
	 
6. Get the detail of the secrets from the EKS that you've created just now

		kubectl get svc
		kubectl get secrets
		kubectl get secret <secret name> -o jsonpath="{['data']['ca\.crt']}" | base64 --decode

7. Create the Admin Token

	Create a file called `eks-admin-service-account.yaml` with contents:

	```
	apiVersion: v1
	kind: ServiceAccount
	metadata:
	  name: eks-admin
	  namespace: kube-system
	```
	Apply the service account to your cluster:

	```
	kubectl apply -f eks-admin-service-account.yaml
	```
	Create a file called `eks-admin-cluster-role-binding.yaml` with contents:

	```
	apiVersion: rbac.authorization.k8s.io/v1beta1
	kind: ClusterRoleBinding
	metadata:
	  name: eks-admin
	roleRef:
	  apiGroup: rbac.authorization.k8s.io
	  kind: ClusterRole
	  name: cluster-admin
	subjects:
	- kind: ServiceAccount
	  name: eks-admin
	  namespace: kube-system
	```

	Apply the cluster role binding to your cluster:

	```
	kubectl apply -f eks-admin-cluster-role-binding.yaml
	```

	Retrieve the token for the `eks-admin` service account:

	```
	kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')
	```

8. The API server endpoint is also required, so GitLab can connect to the cluster. This is displayed on the AWS EKS console, when viewing the EKS cluster details.
9. Open the Kubernetes Configuration on GitLab. Input the API (taken from AWS console). Input the Secrets (taken from command above). Input the Tokens (taken from command above). Disable RBAC, since this is only Development environment.  Then follow up with this command to effectively disable RBAC. Disabling RBAC means that any application running in the cluster, or user who can authenticate to the cluster, has full API access. This is a [security concern](../index.md#security-implications), and may not be desirable. To effectively disable RBAC, global permissions can be applied granting full access:

	```
	kubectl create clusterrolebinding permissive-binding \
	 --clusterrole=cluster-admin \
	 --user=admin \
	 --user=kubelet \
	 --group=system:serviceaccounts
	 ```

10. Let's Go Build!

### III. Prepare Helm, Tiller, Ingress, Prometheus, Persistent Storage

13. Using the Wizard after the configuration earlier. First, install Helm Tiller, a package manager for Kubernetes. This enables deployment of the other applications.
14. Next, if you would like the deployed app to be reachable on the internet, deploy the Ingress. Note that this will also cause an [Elastic Load Balancer](https://aws.amazon.com/documentation/elastic-load-balancing/) to be created, which will incur additional AWS costs.

	To get the DNS name of the load balancer that has been created on AWS, run:
	```
	kubectl get service ingress-nginx-ingress-controller -n gitlab-managed-apps -o jsonpath="{.status.loadBalancer.ingress[0].hostname}"
	```

15. If the project is on GitLab.com, free shared Runners are available and you do not have to deploy one. If a project specific Runner is desired, or there are no shared Runners, it is easy to deploy one. Simply click on the **Install** button for the GitLab Runner.
16. GitLab is able to monitor applications automatically, utilizing [Prometheus](../../integrations/prometheus.html). Kubernetes container CPU and memory metrics are automatically collected, and response metrics are retrieved from NGINX Ingress as well. To enable monitoring, simply install Prometheus into the cluster with the **Install** button.
17. Amazon EKS doesn't have a default Storage Class out of the box, which means requests for persistent volumes will not be automatically fulfilled. As part of Auto DevOps, the deployed Postgres instance requests persistent storage, and without a default storage class it will fail to start. If a default Storage Class doesn't already exist and is desired, follow Amazon's [guide on storage classes](https://docs.aws.amazon.com/eks/latest/userguide/storage-classes.html) to create one.

### IV. Configure Kubernetes Dashboard and Simple Guestbook Application

18. Deploy the Kubernetes dashboard to your cluster:

	```
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/heapster.yaml
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/influxdb.yaml
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
	```
	```
	kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')
	kubectl proxy
	```
	Open the following link with a web browser to access the dashboard endpoint:[http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login)
	Choose **Token**, paste the _`<authentication_token>`_ output from the previous command into the **Token** field, and choose **SIGN IN**.

19. Install Kubernetes Metric Server. The Kubernetes metrics server is an aggregator of resource usage data in your cluster, and it is not deployed by default in Amazon EKS clusters.
	```
	DOWNLOAD_URL=$(curl --silent "https://api.github.com/repos/kubernetes-incubator/metrics-server/releases/latest" | jq -r .tarball_url)
	DOWNLOAD_VERSION=$(grep -o '[^/v]*$' <<< $DOWNLOAD_URL)
	curl -Ls $DOWNLOAD_URL -o metrics-server-$DOWNLOAD_VERSION.tar.gz
	mkdir metrics-server-$DOWNLOAD_VERSION
	tar -xzf metrics-server-$DOWNLOAD_VERSION.tar.gz --directory metrics-server-$DOWNLOAD_VERSION --strip-components 1
	kubectl apply -f metrics-server-$DOWNLOAD_VERSION/deploy/1.8+/

	kubectl get deployment metrics-server -n kube-system

	```
20. Launch a simple Guest Book application just to test whether our Kubernetes cluster and the endpoint is working properly or not ([https://docs.aws.amazon.com/eks/latest/userguide/eks-guestbook.html](https://docs.aws.amazon.com/eks/latest/userguide/eks-guestbook.html)) 
	```
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/redis-master-controller.json
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/redis-master-service.json
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/redis-slave-controller.json
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/redis-slave-service.json
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/redis-slave-service.json
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/guestbook-controller.json
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook-go/guestbook-service.json
	kubectl get services -o wide
	kubectl delete rc/redis-master rc/redis-slave rc/guestbook svc/redis-master svc/redis-slave svc/guestbook
	```



Kind Regards,
Doddi Priyambodo
# Let's Go Build! 

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTMzNDUxNTgxM119
-->